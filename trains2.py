class Train:
    def __init__(self, capacity, destination, letters):
        self.capacity = capacity
        self.destination = destination
        self.letters = letters


class Letter:
    def __init__(self, weight, destination):
        self.weight = weight
        self.destination = destination


class Station:
    def __init__(self, letters):
        self.letters = letters


def process_trains(trains, station):
    trains_done, station_done, _ = process_trains_helper([], trains, station)
    return trains_done, station_done


def process_trains_helper(trains_done, trains, station):
    if trains == []:
        return trains_done, station, trains
    else:
        train = trains[0]
        remaining = trains[1:]
        train_done, station_done = process_train(train, station)
        trains_done.append(train_done)
        return process_trains_helper(trains_done, remaining, station_done)


def process_train(train, station):
    letters = station.letters
    capacity = train.capacity
    destination = train.destination
    train_letters, station_letters = letter_splitter(
        [], [], letters, 0.0, capacity, destination
    )
    return Train(capacity, destination, train_letters), Station(station_letters)


def letter_splitter(train_l, station_l, letters, weight, capacity, destination):
    if letters == []:
        return (train_l, station_l)
    elif letters[0].weight + weight > capacity or letters[0].destination != destination:
        station_l.append(letters[0])
        return letter_splitter(
            train_l, station_l, letters[1:], weight, capacity, destination
        )
    else:
        train_l.append(letters[0])
        return letter_splitter(
            train_l,
            station_l,
            letters[1:],
            weight + letters[0].weight,
            capacity,
            destination,
        )


def read_trains(file):
    with open(file, "r") as f:
        trains = [_get_train(line) for line in f if line[0] != "#"]
    return trains


def _get_train(line):
    dest = line[0]
    cap = float(line[2:]) * 1000.0  # file is in tons
    return Train(cap, dest, [])


def read_letters(file):
    with open(file, "r") as f:
        letters = [_get_letter(line) for line in f if line[0] != "#"]
    return letters


def _get_letter(line):
    dest = line[0]
    weigt = float(line[2:])
    return Letter(weigt, dest)


trains = read_trains("trains.txt")
station = Station(read_letters("parcels.txt")[:90])

trains_processed, station_processed = process_trains(trains, station)
for train in trains_processed:
    print("Train to ", train.destination, "with", len(train.letters), "letters")
