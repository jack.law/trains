class Train:
    def __init__(self, capacity, destination, letters):
        self.capacity = capacity
        self.destination = destination
        self.letters = letters


class Letter:
    def __init__(self, weight, destination):
        self.weight = weight
        self.destination = destination


class Station:
    def __init__(self, letters):
        self.letters = letters


def fold_pure(cum, li, func):
    if li == []:
        return cum
    else:
        return func(li[0], fold(cum, li[1:], func))


def fold(cum, li, func):
    for item in reversed(li):
        cum = func(item, cum)
    return cum


def list_add_pure(item, li):
    return li + [item]


def list_add(item, li):
    li.append(item)
    return li


def process_trains(trains, station):
    return fold(([], station), trains, add_train)


def add_train(train, result):
    trains, station = result
    train_with_letters, station_without_letters = process_train(train, station)
    return list_add(train_with_letters, trains), station_without_letters


def process_train(train, station):
    letters = station.letters
    capacity = train.capacity
    destination = train.destination
    add_letter_to = lambda x, y: add_letter(x, y, capacity, destination)
    train_l, station_l, _ = fold(([], [], 0.0), letters, add_letter_to)
    return Train(capacity, destination, train_l), Station(station_l)


def add_letter(letter, result, capacity, destination):
    train_l, station_l, weight = result
    if letter.destination != destination or letter.weight + weight > capacity:
        return (train_l, list_add(letter, station_l), weight)
    else:
        return (list_add(letter, train_l), station_l, weight + letter.weight)


def read_trains(file):
    with open(file, "r") as f:
        trains = [_get_train(line) for line in f if line[0] != "#"]
    return trains


def _get_train(line):
    dest = line[0]
    cap = float(line[2:]) * 1000.0  # file is in tons
    return Train(cap, dest, [])


def read_letters(file):
    with open(file, "r") as f:
        letters = [_get_letter(line) for line in f if line[0] != "#"]
    return letters


def _get_letter(line):
    dest = line[0]
    weigt = float(line[2:])
    return Letter(weigt, dest)


trains = read_trains("trains.txt")
trains.reverse()
station = Station(read_letters("parcels.txt"))

trains_processed, station_processed = process_trains(trains, station)
for train in trains_processed:
    print("Train to ", train.destination, "with", len(train.letters), "letters")

total_letters = fold(
    0, trains_processed, lambda item, result: result + len(item.letters)
)

print("Total letters ", total_letters)
