class Train:
    def __init__(self, capacity, destination):
        self.capacity = capacity
        self.destination = destination
        self.letters = []


class Letter:
    def __init__(self, weight, destination):
        self.weight = weight
        self.destination = destination


class Station:
    def __init__(self, letters):
        self.letters = self._sort_letters(letters)

    def process_train(self, train):
        """Move letters from the station onto the train, until full"""
        valid_letters = self.letters[train.destination]
        total_weight = 0
        remaining_letters = []
        for letter in valid_letters:
            if total_weight + letter.weight > train.capacity:
                remaining_letters.append(letter)
            else:
                train.letters.append(letter)
                total_weight += letter.weight
        self.letters[train.destination] = remaining_letters

    @staticmethod
    def _sort_letters(letters):
        letters_dict = {}
        for dest in "ABCDE":
            letters_dict[dest] = [
                letter for letter in letters if letter.destination == dest
            ]
        return letters_dict


def read_trains(file):
    with open(file, "r") as f:
        trains = [_get_train(line) for line in f if line[0] != "#"]
    return trains


def _get_train(line):
    dest = line[0]
    cap = float(line[2:]) * 1000.0  # file is in tons
    return Train(cap, dest)


def read_letters(file):
    with open(file, "r") as f:
        letters = [_get_letter(line) for line in f if line[0] != "#"]
    return letters


def _get_letter(line):
    dest = line[0]
    weigt = float(line[2:])
    return Letter(weigt, dest)


trains = read_trains("trains.txt")
station = Station(read_letters("parcels.txt"))

total_letters = 0

for train in trains:
    station.process_train(train)
    total_letters += len(train.letters)
    print("Train to ", train.destination, "with", len(train.letters), "letters")

print("Total letters ", total_letters)
